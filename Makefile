TAG?=16-alpine

docker:
	docker build \
		-t node-pnpm:${TAG} \
		--build-arg TAG=${TAG} \
		--no-cache \
		.